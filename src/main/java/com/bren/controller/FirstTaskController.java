package com.bren.controller;

public interface FirstTaskController {
    void printAverage(int firstValue, int secondValue, int thirdValue);
    void printMaxValue(int firstValue, int secondValue, int thirdValue);
}
