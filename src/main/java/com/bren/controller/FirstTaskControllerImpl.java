package com.bren.controller;

import com.bren.model.FirstTaskBusinessLogic;
import com.bren.model.FirstTaskModel;

public class FirstTaskControllerImpl implements FirstTaskController {

    private FirstTaskModel firstTaskModel;

    public FirstTaskControllerImpl() {
        firstTaskModel = new FirstTaskBusinessLogic();
    }

    @Override
    public void printAverage(int firstValue, int secondValue, int thirdValue) {
        firstTaskModel.printAverage(firstValue,secondValue,thirdValue);
    }

    @Override
    public void printMaxValue(int firstValue, int secondValue, int thirdValue) {
        firstTaskModel.printMaxValue(firstValue,secondValue,thirdValue);
    }
}
