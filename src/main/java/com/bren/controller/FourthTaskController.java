package com.bren.controller;

import java.util.List;

public interface FourthTaskController {
    void printInformation(List<String> words);
}
