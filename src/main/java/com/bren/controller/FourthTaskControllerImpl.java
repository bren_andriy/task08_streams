package com.bren.controller;

import com.bren.model.FirstTaskBusinessLogic;
import com.bren.model.FirstTaskModel;
import com.bren.model.FourthTaskBusinessLogic;
import com.bren.model.FourthTaskModel;

import java.util.List;

public class FourthTaskControllerImpl implements FourthTaskController {

    private FourthTaskModel fourthTaskModel;

    public FourthTaskControllerImpl() {
        fourthTaskModel = new FourthTaskBusinessLogic();
    }

    @Override
    public void printInformation(List<String> words) {
        fourthTaskModel.printInformation(words);
    }
}
