package com.bren.controller;

import com.bren.model.ThirdTaskBusinessLogic;
import com.bren.model.ThirdTaskModel;

public class ThirdTaskControllerImpl implements ThirdTaskController {

    private ThirdTaskModel thirdTaskModel;

    public ThirdTaskControllerImpl() {
        thirdTaskModel = new ThirdTaskBusinessLogic();
    }

    @Override
    public void printStats() {
        thirdTaskModel.printStats();
    }
}
