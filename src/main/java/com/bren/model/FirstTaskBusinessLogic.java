package com.bren.model;

public class FirstTaskBusinessLogic implements FirstTaskModel {

    private FirstTaskService firstTaskService;

    public FirstTaskBusinessLogic() {
        firstTaskService = new FirstTaskService();
    }

    @Override
    public void printAverage(int firstValue, int secondValue, int thirdValue) {
        firstTaskService.printAverage(firstValue,secondValue,thirdValue);
    }

    @Override
    public void printMaxValue(int firstValue, int secondValue, int thirdValue) {
        firstTaskService.printMaxValue(firstValue,secondValue,thirdValue);
    }
}
