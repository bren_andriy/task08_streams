package com.bren.model;

public interface FirstTaskModel {
    void printAverage(int firstValue, int secondValue, int thirdValue);
    void printMaxValue(int firstValue, int secondValue, int thirdValue);
}
