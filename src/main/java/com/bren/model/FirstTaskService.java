package com.bren.model;

import com.bren.first_subtask.Test;

import java.util.stream.Stream;

class FirstTaskService {

    void printAverage(int firstValue, int secondValue, int thirdValue) {
        Test average = (a, b, c) -> (a + b + c) / 3;
        System.out.println(average.method(firstValue, secondValue, thirdValue));
    }

    void printMaxValue(int firstValue, int secondValue, int thirdValue) {
        Integer maxValue = Stream.of(firstValue, secondValue, thirdValue).mapToInt(v -> v).max().orElse(0);
        System.out.println(maxValue);
    }
}
