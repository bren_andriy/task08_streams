package com.bren.model;

import java.util.List;

public class FourthTaskBusinessLogic implements FourthTaskModel {

    private FourthTaskService fourthTaskService;

    public FourthTaskBusinessLogic() {
        fourthTaskService = new FourthTaskService();
    }

    @Override
    public void printInformation(List<String> words) {
        fourthTaskService.printInformation(words);
    }
}
