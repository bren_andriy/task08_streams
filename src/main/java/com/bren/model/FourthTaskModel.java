package com.bren.model;

import java.util.List;

public interface FourthTaskModel {
    void printInformation(List<String> words);
}
