package com.bren.model;

@FunctionalInterface
public interface TestFirst {
    int method(int firstValue, int secondValue, int thirdValue);
}
