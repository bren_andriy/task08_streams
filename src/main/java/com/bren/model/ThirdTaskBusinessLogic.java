package com.bren.model;

public class ThirdTaskBusinessLogic implements ThirdTaskModel {

    private ThirdTaskService thirdTaskService;

    public ThirdTaskBusinessLogic() {
        thirdTaskService = new ThirdTaskService();
    }

    @Override
    public void printStats() {
        thirdTaskService.printStats();
    }
}
