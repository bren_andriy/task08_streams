package com.bren.model;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;

class ThirdTaskService {
    private final static Random random = new Random();

    private int getRandomIntegerInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("Maximum value must be greater than minimum");
        }
        return random.nextInt((max - min) + 1) + min;
    }

    private long getCountNumberBiggerThanAverage() {
        Integer[] array = getArray();
        System.out.println(Arrays.toString(array));
        double average2 = Arrays.stream(array).mapToDouble(value -> value).average().orElse(0);
        System.out.println("Average number: " + average2);
        return Arrays.stream(array)
                .filter(value -> value > average2)
                .count();
    }

    void printStats() {
        System.out.println(getStats());
        System.out.println("==============");
        System.out.println("Number of values that are bigger than average: "
                + getCountNumberBiggerThanAverage());
    }

    private IntSummaryStatistics getStats() {
        List<Integer> list = Arrays.asList(getArray());
        System.out.println(list);
        return list.stream()
                .mapToInt((value) -> value)
                .summaryStatistics();
    }

    private Integer[] getArray() {
        int size = 1 + random.nextInt(10);
        Integer[] array = new Integer[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = getRandomIntegerInRange(5, 100);
        }
        return array;
    }
}
