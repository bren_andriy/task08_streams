package com.bren.second_subtask;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Car car = new Car("Lviv", "A-95", "Skillful handles");
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("1 - Start car\n2 - Stop car\n" +
                    "3 - Refuel car\n4 - Repair car\n");
            System.out.println("Input string: ");
            String string = sc.nextLine();
            System.out.println("Input name of method: ");
            String chooseMethod = sc.nextLine();

            User user = new User(
                    () -> car.start(string),
                    car::stop,
                    new Command() {
                        @Override
                        public void execute() {
                            car.refuel(string);
                        }
                    },
                    new RepairCommand(car)
            );

            switch (chooseMethod) {
                case "1":
                    user.startCar();
                    break;
                case "2":
                    user.stopCar();
                    break;
                case "3":
                    user.refuelCar();
                    break;
                case "4":
                    user.repairCar();
                    break;
            }
        }
    }
}

