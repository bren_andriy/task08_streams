package com.bren.second_subtask;

class Car  {
    private String city;
    private String fuel;
    private String carWorkshop;

    Car(String city, String fuel, String carWorkshop) {
        this.city = city;
        this.fuel = fuel;
        this.carWorkshop = carWorkshop;
    }

    void start(String city) {
        System.out.println("Car start from " + city);
    }

    void stop() {
        System.out.println("Car has stopped");
    }

    void refuel(String fuel) {
        System.out.println("Car has been refueled with " + fuel);
    }

    void repair(String carWorkshop) {
        System.out.println("Car has been repaired in " + carWorkshop);
    }

    String getCity() {
        return city;
    }

    String getFuel() {
        return fuel;
    }

    String getCarWorkshop() {
        return carWorkshop;
    }


}
