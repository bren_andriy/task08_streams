package com.bren.second_subtask;

@FunctionalInterface
public interface Command {
    void execute();
}
