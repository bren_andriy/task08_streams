package com.bren.second_subtask;

public class RefuelCommand implements Command {
    private Car car;

//    RefuelCommand(Car car) {
//        this.car = car;
//    }

    @Override
    public void execute() {
        car.refuel(car.getFuel());

    }

}
