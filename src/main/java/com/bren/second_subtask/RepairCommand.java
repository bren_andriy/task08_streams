package com.bren.second_subtask;

public class RepairCommand implements Command {
    private Car car;

    RepairCommand(Car car) {
        this.car = car;
    }

    @Override
    public void execute() {
        car.repair(car.getCarWorkshop());
    }
}
