package com.bren.second_subtask;

public class StartCommand implements Command {
    private Car car;

//    StartCommand(Car car) {
//        this.car = car;
//    }

    @Override
    public void execute() {
        car.start(car.getCity());
    }
}
