package com.bren.second_subtask;

public class StopCommand implements Command {
    private Car car;

//    StopCommand(Car car) {
//        this.car = car;
//    }

    @Override
    public void execute() {
        car.stop();
    }
}
