package com.bren.second_subtask;

class User  {
    private Command start;
    private Command stop;
    private Command refuel;
    private Command repair;

    User(Command start, Command stop, Command refuel, Command repair) {
        this.start = start;
        this.stop = stop;
        this.refuel = refuel;
        this.repair = repair;
    }

    void startCar() {
        start.execute();
    }

    void stopCar() {
        stop.execute();
    }

    void refuelCar() {
        refuel.execute();
    }

    void repairCar() {
        repair.execute();
    }

}
