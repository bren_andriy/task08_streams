package com.bren.view;

import com.bren.controller.*;


import java.util.*;

public class View {
    private FirstTaskController firstTaskController;
    private ThirdTaskController thirdTaskController;
    private FourthTaskController fourthTaskController;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public View() {
        firstTaskController = new FirstTaskControllerImpl();
        thirdTaskController = new ThirdTaskControllerImpl();
        fourthTaskController = new FourthTaskControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Find average value (task1)");
        menu.put("2", "  2 - Find max value (task1)");
        menu.put("3", "  3 - Print states (task3)");
        menu.put("4", "  4 - Information about words (task4)");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        System.out.println("Input first number: ");
        int firstNumber = input.nextInt();
        System.out.println("Input second number: ");
        int secondNumber = input.nextInt();
        System.out.println("Input third number: ");
        int thirdNumber = input.nextInt();
        System.out.print("Average value: ");
        firstTaskController.printAverage(firstNumber, secondNumber, thirdNumber);
    }

    private void pressButton2() {
        System.out.println("Input first number: ");
        int firstNumber = input.nextInt();
        System.out.println("Input second number: ");
        int secondNumber = input.nextInt();
        System.out.println("Input third number: ");
        int thirdNumber = input.nextInt();
        System.out.println("Max value: ");
        firstTaskController.printMaxValue(firstNumber, secondNumber, thirdNumber);
    }

    private void pressButton3() {
        thirdTaskController.printStats();
    }

    private void pressButton4() {
        System.out.println("============");
        List<String> words = new ArrayList<>();
        String string;
        do {
            System.out.println("Input word: ");
            string = input.nextLine();
            if (!string.equals("")) {
                words.add(string);
            }
        } while (!string.equals(""));
        System.out.println("Initial words list \n" + words);
        fourthTaskController.printInformation(words);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
